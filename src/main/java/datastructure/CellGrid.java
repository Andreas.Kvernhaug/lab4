package datastructure;

import cellular.CellState;
import java.util.Arrays;

public class CellGrid implements IGrid {

    /** Number of rows in grid */
    private int rows;
    /** Number of columns in grid */
    private int cols;
    /** 2D array of object type CellState */
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        for (CellState[] row: this.grid){
            Arrays.fill(row, initialState);
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row >= 0 && row < this.numRows() && column >= 0 && column < this.numColumns()) {
            this.grid[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                newGrid.set(i, j, this.get(i, j));
            }
        }

        return newGrid;
    }
    
}
